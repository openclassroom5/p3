# OCR_P3
Openclassroom || Projet 3 : Ohmyfood

[ FR ]

// Scénario
Vous venez d’être recruté chez Ohmyfood!, en tant que développeur junior. Félicitations !

Ohmyfood! est une jeune startup qui voudrait s'imposer sur le marché de la restauration. L'objectif est de développer un site 100% mobile qui répertorie les menus de restaurants gastronomiques. En plus des systèmes classiques de réservation, les clients pourront composer le menu de leur repas pour que les plats soient prêts à leur arrivée. Finis, les temps d'attente au restaurant !

[ ENG ]

// Script
You have just been recruited at Ohmyfood!, as a junior developer. Congratulation !

Ohmyfood! is a young startup that would like to impose itself on the catering market. The goal is to develop a 100% mobile site that lists the menus of gourmet restaurants. In addition to traditional reservation systems, customers can compose their meal menu so that the dishes are ready when they arrive. No more waiting times at the restaurant!



